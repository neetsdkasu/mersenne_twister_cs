﻿/* Mersenne Twister 移植
 * Leonardone @ NEETSDKASU
 * BSD-2-Clause License
 */
using System;

namespace Mt19937ar
{
    public class MersenneTwister
    {
        private const int N = 624;
        private const int M = 397;
        private const uint MatrixA = 0x9908b0dfu;
        private const uint UpperMask = 0x80000000u;
        private const uint LowerMask = 0x7fffffffu;
        private const uint DefaultSeed = 5489u;

        private static readonly uint[] Mag01 = { 0, MersenneTwister.MatrixA };

        private uint[] mt = new uint[MersenneTwister.N];
        private int mti;

        public MersenneTwister() : this(MersenneTwister.DefaultSeed) { }

        public MersenneTwister(uint seed)
        {
            this.SetSeed(seed);
        }

        public MersenneTwister(uint[] key)
        {
            this.SetSeed(key);
        }

        /* initializes mt[N] with a seed */
        public void SetSeed(uint seed)
        {
            this.mt[0] = seed;
            for (var i = 1; i < MersenneTwister.N; i++)
            {
                this.mt[i] = 1812433253u * (this.mt[i - 1] ^ (this.mt[i - 1] >> 30)) + (uint)i;
            }
            this.mti = MersenneTwister.N;
        }

        /* initialize by an array */
        public void SetSeed(uint[] key)
        {
            this.SetSeed(19650218u);
            var i = 1;
            var j = 0;
            for (var k = Math.Max(key.Length, MersenneTwister.N); k > 0; k--)
            {
                this.mt[i] = (this.mt[i] ^ ((this.mt[i - 1] ^ (this.mt[i - 1] >> 30)) * 1664525u)) + key[j] + (uint)j;
                i++;
                if (i >= MersenneTwister.N)
                {
                    this.mt[0] = this.mt[MersenneTwister.N - 1];
                    i = 1;
                }
                j++;
                if (j >= key.Length)
                {
                    j = 0;
                }
            }
            for (var k = MersenneTwister.N - 1; k > 0; k--)
            {
                this.mt[i] = (this.mt[i] ^ ((this.mt[i - 1] ^ (this.mt[i - 1] >> 30)) * 1566083941u)) - (uint)i;
                i++;
                if (i >= MersenneTwister.N)
                {
                    this.mt[0] = this.mt[MersenneTwister.N - 1];
                    i = 1;
                }
            }
            this.mt[0] = 0x80000000u;
        }

        /* generates a random number on [0,0xffffffff]-interval */
        public uint GenrandUint32()
        {
            uint y;

            if (this.mti >= MersenneTwister.N) /* generate N words at one time */
            {
                int kk;

                for (kk = 0; kk < MersenneTwister.N - MersenneTwister.M; kk++)
                {
                    y = (this.mt[kk] & MersenneTwister.UpperMask) | (this.mt[kk + 1] & MersenneTwister.LowerMask);
                    this.mt[kk] = this.mt[kk + MersenneTwister.M] ^ (y >> 1) ^ MersenneTwister.Mag01[y & 0x1];
                }
                for (; kk < MersenneTwister.N - 1; kk++)
                {
                    y = (this.mt[kk] & MersenneTwister.UpperMask) | (this.mt[kk + 1] & MersenneTwister.LowerMask);
                    this.mt[kk] = this.mt[kk + (MersenneTwister.M - MersenneTwister.N)] ^ (y >> 1) ^ MersenneTwister.Mag01[y & 0x1];
                }
                y = (this.mt[MersenneTwister.N - 1] & MersenneTwister.UpperMask) | (this.mt[0] & MersenneTwister.LowerMask);
                this.mt[MersenneTwister.N - 1] = this.mt[MersenneTwister.M - 1] ^ (y >> 1) ^ MersenneTwister.Mag01[y & 0x1];
                this.mti = 0;
            }

            y = this.mt[this.mti];
            this.mti++;

            /* Tempering */
            y ^= y >> 11;
            y ^= (y << 7) & 0x9d2c5680u;
            y ^= (y << 15) & 0xefc60000u;
            y ^= y >> 18;

            return y;
        }

        /* generates a random number on [0,0x7fffffff]-interval */
        public uint GenrandUint31()
        {
            return this.GenrandUint32() >> 1;
        }

        /* generates a random number on [0,1]-real-interval */
        public double GenrandReal1()
        {
            return (double)this.GenrandUint32() * (1.0 / 4294967295.0);
            /* divided by 2^32-1 */
        }

        /* generates a random number on [0,1)-real-interval */
        public double GenrandReal2()
        {
            return (double)this.GenrandUint32() * (1.0 / 4294967296.0);
            /* divided by 2^32 */
        }

        /* generates a random number on (0,1)-real-interval */
        public double GenrandReal3()
        {
            return ((double)this.GenrandUint32() + 0.5) * (1.0 / 4294967296.0);
            /* divided by 2^32 */
        }

        /* generates a random number on [0,1) with 53-bit resolution*/
        public double GenrandRes53()
        {
            var a = this.GenrandUint32() >> 5;
            var b = this.GenrandUint32() >> 6;
            return ((double)a * 67108864.0 + (double)b) * (1.0 / 9007199254740992.0);
        }
    }
}

/*
 *  疑似乱数生成機(RNG)  移植(Porting)
 *  Information of Original Source
 *  Mersenne Twister with improved initialization (2002)
 *  http://www.math.sci.hiroshima-u.ac.jp/~m-mat/MT/mt.html
 *  http://www.math.sci.hiroshima-u.ac.jp/~m-mat/MT/MT2002/mt19937ar.html
 */
// = 移植元ラインセンス (License of Original Source) =======================================================
// ======================================================================
/*
   A C-program for MT19937, with initialization improved 2002/1/26.
   Coded by Takuji Nishimura and Makoto Matsumoto.

   Before using, initialize the state by using init_genrand(seed)
   or init_by_array(init_key, key_length).

   Copyright (C) 1997 - 2002, Makoto Matsumoto and Takuji Nishimura,
   All rights reserved.

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions
   are met:

     1. Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.

     2. Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.

     3. The names of its contributors may not be used to endorse or promote
        products derived from this software without specific prior written
        permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
   CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


   Any feedback is very welcome.
   http://www.math.sci.hiroshima-u.ac.jp/~m-mat/MT/emt.html
   email: m-mat @ math.sci.hiroshima-u.ac.jp (remove space)
*/
// ======================================================================